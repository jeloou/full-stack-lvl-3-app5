module.exports = (function(app, db) {
  var todos = db.collection('todos');
  
  app.route('/todos')
    .all(function(req, res, next) {
      if (!req.user) return res.status(401).end();
      next();
    })
    .post(function(req, res) {
      var todo;

      todo = JSON.parse(req.body.model);
      res.json(todo);
    })
    .get(function(req, res) {
      res.json(todos);
    });

  app.route('/todos/:id')
    .all(function(req, res, next) {
      if (!req.user) return res.status(401).end();
      next();
    })
    .get(function(req, res) {
      var id;

      id = req.params.id;
      res.json(todo);
    })
    .put(function(req, res) {
      var todo,
	  id;

      id = req.params.id;
      todo = JSON.parse(req.body.model);
      res.json(todo);
    })
    .delete(function(req, res) {
      var todo,
	  id;

      id = req.params.id;
      res.json(todo);
    });  
});
