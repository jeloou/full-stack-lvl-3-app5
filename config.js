var express = require('express'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    MongoClient = require('mongodb').MongoClient,
    bodyparser = require('body-parser'),
    session = require('express-session'),
    path = require('path'),
    util = require('util');

module.exports = exports = function(app, passport, fn) {
  /*
    MongoDb configuration
  */
  var DATABASE_HOST,
      DATABASE_NAME,
      DATABASE_USER,
      DATABASE_PASSWORD;

  switch (app.settings.env) {
  case 'production':
    DATABASE_HOST = process.env.DATABASE_HOST;
    DATABASE_NAME = process.env.DATABASE_NAME;
    DATABASE_USER = process.env.DATABASE_USER;
    DATABASE_PASSWORD = process.env.DATABASE_PASSWORD;
    break;

  case 'development':
    DATABASE_HOST = 'localhost';
    DATABASE_NAME = 'db';
    break;

  default:
    break;
  }

  var DATABASE_URL = util.format(
    'mongodb://%s/%s', DATABASE_HOST, DATABASE_NAME);

  /*
    Sessions configuration
  */
  var SESSION_ID_KEY,
      COOKIE_SECRET;

  switch(app.settings.env) {
  case 'production':
    SESSION_ID_KEY = process.env.SESSION_ID_KEY;
    COOKIE_SECRET = process.env.COOKIE_SECRET;
    break;

  case 'development':
    SESSION_ID_KEY = 'macaroon'
    COOKIE_SECRET = 'very secret string';
    break;

  default:
    break;
  }

  /* 
     .bodyParser should be above .methodOverride
  */
  app.use(bodyparser.urlencoded());
  app.use(session({
    secret: COOKIE_SECRET,
    name: SESSION_ID_KEY,
    saveUninitialized: true,
    resave: true
  }));

  /*
    Configuring passport
  */
  var users = [
    {id: 1, username: 'joseph', password: 'password'},
    {id: 2, username: 'guest', password: 'password'}
  ];

  passport.use('local', new LocalStrategy(
    function(username, password, done) {
      var user, i;

      for (i = 0; i < users.length; i++) {
	if (users[i].username == username) {
	  break;
	}
      }

      user = users[i];
      if (user.username !== username) {
	return done(null, false, {message: 'Username doesn\'t exist'});
      }

      if (user.password !== password) {
	return done(null, false, {message: 'Invalid password'});
      }
      return done(null, user);
    }
  ));

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    var user, i;

    for (i = 0; i < users.length; i++) {
      if (users[i].id === id) {
	break;
      }
    }

    user = users[i];
    done(null, user);
  });

  app.use(passport.initialize());
  app.use(passport.session());

  /*
    Adding favicon and serving compressed static files
  */
  app.use(express.static(path.join(__dirname, 'public')));

  /*
    Configuring views dir and engine
  */
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'jade');

  MongoClient.connect(DATABASE_URL, function(err, db) {
    if (err) throw err;
    fn(db);
  });
};
